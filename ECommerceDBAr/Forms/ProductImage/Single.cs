﻿using System;
using System.Linq;
using System.Windows.Forms;
using Castle.Components.Validator;

namespace ECommerceDBAr.Forms.ProductImage
{
    public partial class Single : Form
    {
        public Models.ProductImage ProductImage;
        public AutoCompleteStringCollection TagsAutoCompleteSource = new AutoCompleteStringCollection();
        public Single(Models.ProductImage productImage)
        {
            InitializeComponent();
            ProductImage = productImage;
            locationTextBox.Text = ProductImage.ImageLocation;
            titleTextBox.Text = ProductImage.Title;
            TagsAutoCompleteSource.AddRange(Models.Tag.FindAll().ToList().Select(x => x.Name).ToArray());
            addNewTagTextBox.AutoCompleteCustomSource = TagsAutoCompleteSource;
            if (ProductImage.Tags != null)
            {
                PopulateTags();
            }
        }


        public void PopulateTags()
        {
            tagDataGrid.Rows.Clear();
            foreach (Models.Tag tag in ProductImage.Tags)
            {
                tagDataGrid.Rows.Add(tag.Id, tag.Name, "Delete");
            }
        }

        private void saveButton_Click(object sender, System.EventArgs e)
        {
            ProductImage.Title = titleTextBox.Text;
            ProductImage.ImageLocation = locationTextBox.Text;
            if (ProductImage.IsValid())
            {
                ProductImage.SaveAndFlush();
                Close();
            }
            else
            {
                MessageBox.Show(string.Join("\n", ProductImage.ValidationErrorMessages));
            }

        }

        private void addNewTagBtn_Click(object sender, System.EventArgs e)
        {
            if (addNewTagTextBox.Text.Length > 1)
            {
                try
                {
                    ProductImage.Tags.Add(Models.Tag.GetTag(addNewTagTextBox.Text));
                    ProductImage.SaveAndFlush();
                    ProductImage.Refresh();
                    PopulateTags();
                }
                catch (ValidationException exception)
                {
                    MessageBox.Show(string.Join("\n", ProductImage.ValidationErrorMessages));
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Adatbázis hiba, valószinű hogy ez a tag már hozzá van rendelve a termékhez!");
                }
            }
        }

        private void tagDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.RowIndex <= tagDataGrid.RowCount)
                {
                    ProductImage.Tags.RemoveAt(e.RowIndex);
                    ProductImage.SaveAndFlush();
                    ProductImage.Refresh();
                    PopulateTags();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            ProductImage.DeleteAndFlush();
            Close();
        }
    }
}
