﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Castle.Components.Validator;


namespace ECommerceDBAr.Forms.Product
{
    public partial class Single : Form
    {
        public Models.Product Product;
        public Dictionary<int, string> Categories = new Dictionary<int, string>();
        public AutoCompleteStringCollection TagsAutoCompleteSource = new AutoCompleteStringCollection();
        public Single(Models.Product product)
        {
            InitializeComponent();
            PopulateCategories();
            Product = product;
            nameTextBox.Text = Product.Name;
            priceTextBox.Text = Product.Price.ToString();
            descriptionTextBox.Text = Product.Description;
            categoryComboBox.DataSource = new BindingSource(Categories, null);
            categoryComboBox.DisplayMember = "Value";
            categoryComboBox.ValueMember = "Key";
            TagsAutoCompleteSource.AddRange(Models.Tag.FindAll().ToList().Select(x => x.Name).ToArray());
            addNewTagTextBox.AutoCompleteCustomSource = TagsAutoCompleteSource;
            if (Product.Category != null)
            {
                categoryComboBox.SelectedText = Categories[Product.CategoryId];
                categoryComboBox.SelectedValue = Product.CategoryId;
            }
            if (Product.Tags != null)
            {
                PopulateTags();
            }
            if (Product.Images != null)
            {
                PopulateImages();
            }

        }

        private void PopulateCategories()
        {
            Models.Category[] categories = Models.Category.FindAll();
            foreach (Models.Category category in categories)
            {
                Categories.Add(category.Id, category.Name);
            }
        }

        private void PopulateImages()
        {
            productImageDataGrid.Rows.Clear();
            foreach (Models.ProductImage image in Product.Images)
            {
                productImageDataGrid.Rows.Add(image.Id, image.ImageLocation, image.Title, "Edit");
            }
        }

        private void PopulateTags()
        {
            tagDataGrid.Rows.Clear();
            foreach (Models.Tag tag in Product.Tags)
            {
                tagDataGrid.Rows.Add(tag.Id, tag.Name, "Delete");
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {

                Product.Name = nameTextBox.Text;
                Product.Price = float.Parse(priceTextBox.Text);
                Product.Description = descriptionTextBox.Text;
                Models.Category productCategory =
                    Models.Category.Find(((KeyValuePair<int, string>)categoryComboBox.SelectedItem).Key);
                Product.Category = productCategory;
                Product.CategoryId = (int)categoryComboBox.SelectedValue;
                if (Product.IsValid())
                {
                    Product.SaveAndFlush();
                    Dispose();
                }
                else
                {
                    MessageBox.Show(string.Join("\n", Product.ValidationErrorMessages));
                }

            }
            catch (FormatException)
            {
                MessageBox.Show("Az arhoz csak szamokat irj!");
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.GetType().ToString());
            }
        }

        private void tagDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.RowIndex <= tagDataGrid.RowCount)
                {
                    Product.Tags.RemoveAt(e.RowIndex);
                    Product.SaveAndFlush();
                    Product.Refresh();
                    PopulateTags();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
        }

        private void addNewTagBtn_click(object sender, EventArgs e)
        {
            if (addNewTagTextBox.Text.Length > 1)
            {          
                try
                {
                    Product.Tags.Add(Models.Tag.GetTag(addNewTagTextBox.Text));
                    Product.SaveAndFlush();
                    Product.Refresh();
                    PopulateTags();
                }
                catch (ValidationException exception)
                {
                    MessageBox.Show(string.Join("\n", Product.ValidationErrorMessages));
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Adatbázis hiba, valószinű hogy ez a tag már hozzá van rendelve a termékhez!");
                }
            }
        }

        private void productImageDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.RowIndex <= productImageDataGrid.RowCount)
                {
                    int id = Int32.Parse(productImageDataGrid[0, e.RowIndex].Value.ToString());
                    ProductImage.Single productImageForm = new ProductImage.Single(Models.ProductImage.Find(id));
                    productImageForm.ShowDialog();
                    Product.Refresh();
                    PopulateImages();
                }
            }
            catch (Exception exception)
            {

                MessageBox.Show(exception.ToString());
            }
        }

        private void addNewProductImage_Click(object sender, EventArgs e)
        {
            Models.ProductImage productImage = new Models.ProductImage() {ProductId = Product.Id};
            ProductImage.Single productImageForm = new ProductImage.Single(productImage);
            productImageForm.ShowDialog();
            Product.Refresh();
            PopulateImages();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Product.Delete();
            Close();
        }
    }
}