﻿using System;
using System.Windows.Forms;

namespace ECommerceDBAr.Forms.Product
{
    public partial class List : Form
    {
        public Models.Product[] Products = Models.Product.FindAll();
        public List()
        {
            InitializeComponent();
            FillGrid();
        }

        public void FillGrid()
        {
            dataGrid.Rows.Clear();
            Products = Models.Product.FindAll();
            foreach (Models.Product product in Products)
            {
                dataGrid.Rows.Add(product.Id.ToString(), product.Name, product.Category.Name, "Edit");
            }
        }

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            {
                try
                {
                    if (e.ColumnIndex != 0 && e.RowIndex >= 0 && e.RowIndex <= dataGrid.RowCount)
                    {
                        int id = Int32.Parse(dataGrid[0, e.RowIndex].Value.ToString());
                        Models.Product productModel = Models.Product.Find(id);
                        Single form = new Single(productModel);
                        form.ShowDialog();
                        FillGrid();
                    }
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.ToString());
                }
            }
        }

        private void addNewBtn_Click(object sender, EventArgs e)
        {
            Models.Product productModel = new Models.Product();
            Single form = new Single(productModel);
            form.ShowDialog();
            FillGrid();
        }
    }
}
