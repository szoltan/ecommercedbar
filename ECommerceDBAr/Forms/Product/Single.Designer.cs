﻿namespace ECommerceDBAr.Forms.Product
{
    partial class Single
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.basicInfosTab = new System.Windows.Forms.TabPage();
            this.tagTab = new System.Windows.Forms.TabPage();
            this.addNewTagTextBox = new System.Windows.Forms.TextBox();
            this.addNewTagBtn = new System.Windows.Forms.Button();
            this.tagDataGrid = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditBtn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.imagesTab = new System.Windows.Forms.TabPage();
            this.productImageDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.addNewProductImage = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.basicInfosTab.SuspendLayout();
            this.tagTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tagDataGrid)).BeginInit();
            this.imagesTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productImageDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(6, 7);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(35, 13);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(6, 24);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(496, 20);
            this.nameTextBox.TabIndex = 1;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(6, 51);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(31, 13);
            this.priceLabel.TabIndex = 2;
            this.priceLabel.Text = "Price";
            // 
            // priceTextBox
            // 
            this.priceTextBox.Location = new System.Drawing.Point(6, 68);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(496, 20);
            this.priceTextBox.TabIndex = 3;
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(6, 95);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(49, 13);
            this.categoryLabel.TabIndex = 4;
            this.categoryLabel.Text = "Category";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(6, 112);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(496, 21);
            this.categoryComboBox.TabIndex = 5;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(6, 140);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.descriptionLabel.TabIndex = 6;
            this.descriptionLabel.Text = "Description";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(6, 157);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(496, 126);
            this.descriptionTextBox.TabIndex = 7;
            this.descriptionTextBox.Text = "D";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(454, 12);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.basicInfosTab);
            this.tabControl.Controls.Add(this.tagTab);
            this.tabControl.Controls.Add(this.imagesTab);
            this.tabControl.Location = new System.Drawing.Point(12, 41);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(518, 327);
            this.tabControl.TabIndex = 9;
            // 
            // basicInfosTab
            // 
            this.basicInfosTab.Controls.Add(this.nameTextBox);
            this.basicInfosTab.Controls.Add(this.nameLabel);
            this.basicInfosTab.Controls.Add(this.descriptionTextBox);
            this.basicInfosTab.Controls.Add(this.priceLabel);
            this.basicInfosTab.Controls.Add(this.descriptionLabel);
            this.basicInfosTab.Controls.Add(this.priceTextBox);
            this.basicInfosTab.Controls.Add(this.categoryComboBox);
            this.basicInfosTab.Controls.Add(this.categoryLabel);
            this.basicInfosTab.Location = new System.Drawing.Point(4, 22);
            this.basicInfosTab.Name = "basicInfosTab";
            this.basicInfosTab.Padding = new System.Windows.Forms.Padding(3);
            this.basicInfosTab.Size = new System.Drawing.Size(510, 301);
            this.basicInfosTab.TabIndex = 0;
            this.basicInfosTab.Text = "Basic infos";
            this.basicInfosTab.UseVisualStyleBackColor = true;
            // 
            // tagTab
            // 
            this.tagTab.Controls.Add(this.addNewTagTextBox);
            this.tagTab.Controls.Add(this.addNewTagBtn);
            this.tagTab.Controls.Add(this.tagDataGrid);
            this.tagTab.Location = new System.Drawing.Point(4, 22);
            this.tagTab.Name = "tagTab";
            this.tagTab.Padding = new System.Windows.Forms.Padding(3);
            this.tagTab.Size = new System.Drawing.Size(510, 301);
            this.tagTab.TabIndex = 1;
            this.tagTab.Text = "Tags";
            this.tagTab.UseVisualStyleBackColor = true;
            // 
            // addNewTagTextBox
            // 
            this.addNewTagTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.addNewTagTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.addNewTagTextBox.Location = new System.Drawing.Point(330, 273);
            this.addNewTagTextBox.Name = "addNewTagTextBox";
            this.addNewTagTextBox.Size = new System.Drawing.Size(100, 20);
            this.addNewTagTextBox.TabIndex = 3;
            // 
            // addNewTagBtn
            // 
            this.addNewTagBtn.Location = new System.Drawing.Point(429, 272);
            this.addNewTagBtn.Name = "addNewTagBtn";
            this.addNewTagBtn.Size = new System.Drawing.Size(75, 23);
            this.addNewTagBtn.TabIndex = 2;
            this.addNewTagBtn.Text = "Save";
            this.addNewTagBtn.UseVisualStyleBackColor = true;
            this.addNewTagBtn.Click += new System.EventHandler(this.addNewTagBtn_click);
            // 
            // tagDataGrid
            // 
            this.tagDataGrid.AllowUserToAddRows = false;
            this.tagDataGrid.AllowUserToDeleteRows = false;
            this.tagDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tagDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tagDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.ProductName,
            this.EditBtn});
            this.tagDataGrid.Location = new System.Drawing.Point(6, 6);
            this.tagDataGrid.MultiSelect = false;
            this.tagDataGrid.Name = "tagDataGrid";
            this.tagDataGrid.ReadOnly = true;
            this.tagDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.tagDataGrid.Size = new System.Drawing.Size(498, 256);
            this.tagDataGrid.TabIndex = 1;
            this.tagDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tagDataGrid_CellContentClick);
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // ProductName
            // 
            this.ProductName.HeaderText = "Name";
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            // 
            // EditBtn
            // 
            this.EditBtn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EditBtn.HeaderText = "Delete";
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.ReadOnly = true;
            this.EditBtn.Text = "Delete";
            this.EditBtn.UseColumnTextForButtonValue = true;
            // 
            // imagesTab
            // 
            this.imagesTab.Controls.Add(this.addNewProductImage);
            this.imagesTab.Controls.Add(this.productImageDataGrid);
            this.imagesTab.Location = new System.Drawing.Point(4, 22);
            this.imagesTab.Name = "imagesTab";
            this.imagesTab.Size = new System.Drawing.Size(510, 301);
            this.imagesTab.TabIndex = 2;
            this.imagesTab.Text = "Images";
            this.imagesTab.UseVisualStyleBackColor = true;
            // 
            // productImageDataGrid
            // 
            this.productImageDataGrid.AllowUserToAddRows = false;
            this.productImageDataGrid.AllowUserToDeleteRows = false;
            this.productImageDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.productImageDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.productImageDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Category,
            this.dataGridViewButtonColumn1});
            this.productImageDataGrid.Location = new System.Drawing.Point(3, 3);
            this.productImageDataGrid.MultiSelect = false;
            this.productImageDataGrid.Name = "productImageDataGrid";
            this.productImageDataGrid.ReadOnly = true;
            this.productImageDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.productImageDataGrid.Size = new System.Drawing.Size(504, 263);
            this.productImageDataGrid.TabIndex = 1;
            this.productImageDataGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.productImageDataGrid_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Location";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Category
            // 
            this.Category.HeaderText = "Title";
            this.Category.Name = "Category";
            this.Category.ReadOnly = true;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewButtonColumn1.HeaderText = "Edit";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.ReadOnly = true;
            // 
            // addNewProductImage
            // 
            this.addNewProductImage.Location = new System.Drawing.Point(432, 273);
            this.addNewProductImage.Name = "addNewProductImage";
            this.addNewProductImage.Size = new System.Drawing.Size(75, 23);
            this.addNewProductImage.TabIndex = 2;
            this.addNewProductImage.Text = "Add new";
            this.addNewProductImage.UseVisualStyleBackColor = true;
            this.addNewProductImage.Click += new System.EventHandler(this.addNewProductImage_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(13, 13);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 10;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // Single
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 373);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.saveButton);
            this.Name = "Single";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Single";
            this.tabControl.ResumeLayout(false);
            this.basicInfosTab.ResumeLayout(false);
            this.basicInfosTab.PerformLayout();
            this.tagTab.ResumeLayout(false);
            this.tagTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tagDataGrid)).EndInit();
            this.imagesTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productImageDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage basicInfosTab;
        private System.Windows.Forms.TabPage tagTab;
        private System.Windows.Forms.TabPage imagesTab;
        private System.Windows.Forms.DataGridView tagDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewButtonColumn EditBtn;
        private System.Windows.Forms.Button addNewTagBtn;
        private System.Windows.Forms.TextBox addNewTagTextBox;
        private System.Windows.Forms.DataGridView productImageDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Category;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.Button addNewProductImage;
        private System.Windows.Forms.Button deleteButton;
    }
}