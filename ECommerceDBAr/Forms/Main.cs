﻿using System.Windows.Forms;
namespace ECommerceDBAr.Forms
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void productsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Product.List productListForm = new Product.List();
            productListForm.ShowDialog();
        }

        private void categoriesToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Category.List categoryListForm = new Category.List();
            categoryListForm.ShowDialog();
        }
    }
}
