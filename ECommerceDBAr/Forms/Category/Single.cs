﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Components.Validator;

namespace ECommerceDBAr.Forms.Category
{
    public partial class Single : Form
    {
        public Models.Category Category = null;
        public Single(Models.Category category)
        {
            InitializeComponent();
            Category = category;
            nameTextBox.Text = category.Name;
            descriptionTextBox.Text = category.Description;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                Category.Name = nameTextBox.Text;
                Category.Description = descriptionTextBox.Text;
                if (Category.IsValid())
                {
                    Category.SaveAndFlush();
                    Dispose();
                }
                else
                {
                    MessageBox.Show(string.Join("\n", Category.ValidationErrorMessages));
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }
            finally
            {
                Dispose();
            }
        }
    }
}
