﻿using System;
using System.Windows.Forms;

namespace ECommerceDBAr.Forms.Category
{
    public partial class List : Form
    {
        public Models.Category[] Categories = Models.Category.FindAll();
        public List()
        {
            InitializeComponent();
            FillGrid();
        }
        public void FillGrid()
        {
            dataGrid.Rows.Clear();
            Categories = Models.Category.FindAll();
            foreach (Models.Category category in Categories)
            {
                dataGrid.Rows.Add(category.Id.ToString(), category.Name, "Edit");
            }
        }
        private void addNewButton_Click(object sender, EventArgs e)
        {
            Models.Category categoryModel = new Models.Category();
            Single form = new Single(categoryModel);
            form.ShowDialog();
            FillGrid();
        }

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            {
                try
                {
                    if (e.ColumnIndex != 0 && e.RowIndex >= 0 && e.RowIndex <= dataGrid.RowCount)
                    {
                        int id = Int32.Parse(dataGrid[0, e.RowIndex].Value.ToString());
                        Models.Category categoryModel = Models.Category.Find(id);
                        Single form = new Single(categoryModel);
                        form.ShowDialog();
                        FillGrid();
                    }
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.ToString());
                }
            }
        }
    }
}
