﻿using System.Collections;
using Castle.ActiveRecord;
using Castle.Components.Validator;

namespace ECommerceDBAr.Models
{
    [ActiveRecord("Product")]
    public class Product : ActiveRecordValidationBase<Product>
    {
        [PrimaryKey(PrimaryKeyType.Increment, "id")]
        public int Id { get; set; }

        [Property("name"), ValidateNonEmpty("Nev kotelezo")]
        public string Name { get; set; }

        [Property("price"), ValidateDouble]
        public float Price { get; set; }

        [Property("description"), ValidateNonEmpty("Leiras kotelezo")]
        public string Description { get; set; }

        public int CategoryId
        {
            get { return Category.Id; }
            set { Category = Category.FindByPrimaryKey(value); }
        }

        [BelongsTo("category_id")]
        public Category Category { get; set; }

        [HasAndBelongsToMany(typeof(Tag), Table = "product_tags", ColumnKey = "product_id", ColumnRef = "tag_id")]
        public IList Tags { get; set; } = new ArrayList();


        [HasMany(typeof(ProductImage), Table = "product_image", ColumnKey = "product_id")]
        public IList Images { get; set; } = new ArrayList();
    }
}