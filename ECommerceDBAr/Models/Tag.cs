﻿using Castle.ActiveRecord;
using Castle.Components.Validator;
using NHibernate.Criterion;

namespace ECommerceDBAr.Models
{
    [ActiveRecord("Tag")]
    public class Tag : ActiveRecordValidationBase<Tag>
    {
        [PrimaryKey(PrimaryKeyType.Increment, "id")]
        public int Id { get; set; }

        [Property("name"), ValidateNonEmpty("Nev kotelezo")]
        public string Name { get; set; }

        public static Tag GetTag(string tagName)
        {
            Tag tag = (Tag)Tag.FindFirst(typeof(Tag), Restrictions.Eq("Name", tagName));
            if (tag == null)
            {
                tag = new Tag {Name = tagName};
                tag.Save();
                tag.Refresh();
                return tag;
            }
            return tag;
        }
    }
}