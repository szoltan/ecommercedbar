﻿using System.Collections;
using Castle.ActiveRecord;
using Castle.Components.Validator;

namespace ECommerceDBAr.Models
{
    [ActiveRecord("ProductImage", Table = "product_image")]
    public class ProductImage : ActiveRecordValidationBase<ProductImage>
    {
        [PrimaryKey(PrimaryKeyType.Increment, "id")]
        public int Id { get; set; }

        [Property("image_location"), ValidateNonEmpty("Kep helye kotelezo")]
        public string ImageLocation { get; set; }

        [Property("title"), ValidateNonEmpty("Cim kotelezo")]
        public string Title { get; set; }

        public int ProductId
        {
            get { return Product.Id; }
            set { Product = Product.FindByPrimaryKey(value); }
        }

        [BelongsTo("product_id")]
        public Product Product { get; set; }

        [HasAndBelongsToMany(typeof(Tag), Table = "product_image_tags", ColumnKey = "product_image_id", ColumnRef = "tag_id")]
        public IList Tags { get; set; } = new ArrayList();
    }
}