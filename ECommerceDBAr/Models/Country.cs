﻿using System;
using Castle.ActiveRecord;

namespace ECommerceDBAr.Models
{
    [ActiveRecord("Country")]
    public class Country : ActiveRecordValidationBase<Country>
    {
        [PrimaryKey(PrimaryKeyType.Native, "id")]
        public int Id { get; set; }

        [Property("name")]
        public String Name { get; set; }
    }
}