﻿using Castle.ActiveRecord;
using Castle.Components.Validator;

namespace ECommerceDBAr.Models
{
    [ActiveRecord("Category")]
    public class Category : ActiveRecordValidationBase<Category>
    {
        [PrimaryKey(PrimaryKeyType.Increment, "id")]
        public int Id { get; set; }

        [Property("name"), ValidateNonEmpty("Nev kotelezo")]
        public string Name { get; set; }

        [Property("description"), ValidateNonEmpty("Leiras kotelezo")]
        public string Description { get; set; }
    }
}