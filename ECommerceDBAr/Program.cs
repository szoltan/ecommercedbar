﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;
using ECommerceDBAr.Forms;
using ECommerceDBAr.Models;


namespace ECommerceDBAr
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            InPlaceConfigurationSource source = new InPlaceConfigurationSource();

            IDictionary<string, string> properties = new Dictionary<string, string>();

            properties.Add("hibernate.connection.driver_class", "NHibernate.Driver.OracleClientDriver");
            properties.Add("dialect", "NHibernate.Dialect.Oracle9iDialect");
            properties.Add("connection.provider", "NHibernate.Connection.DriverConnectionProvider");
            properties.Add("connection.driver_class", "NHibernate.Driver.OracleClientDriver");
            properties.Add("proxyfactory.factory_class", "NHibernate.ByteCode.Castle.ProxyFactoryFactory, NHibernate.ByteCode.Castle");
            properties.Add("connection.connection_string", "Data Source=localhost;User ID=hr;Password=hr;");

            source.Add(typeof(ActiveRecordBase), properties);

            ActiveRecordStarter.Initialize(source, typeof(Country), typeof(Product), typeof(Category), typeof(Tag), typeof(ProductImage));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}
